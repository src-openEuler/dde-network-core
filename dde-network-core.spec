Name:           dde-network-core
Version:        1.0.74
Release:        1
Summary:        Deepin desktop-environment - network core files
License:        GPLv3
URL:            https://github.com/linuxdeepin/dde-network-core
Source0:        %{name}-%{version}.tar.gz

BuildRequires:  gcc-c++
BuildRequires:  cmake
BuildRequires:  pkgconfig(dframeworkdbus) >= 2.0

BuildRequires:  pkgconfig(Qt5Core)
BuildRequires:  qt5-qtbase-devel
BuildRequires:  qt5-qtbase-private-devel
BuildRequires:  qt5-linguist
BuildRequires:  pkgconfig(gsettings-qt)
BuildRequires:  qt5-qttools-devel
BuildRequires:  qt5-qtsvg-devel

BuildRequires:  kf5-networkmanager-qt-devel

BuildRequires:  dtkwidget-devel
BuildRequires:  dde-control-center
BuildRequires:  dde-control-center-devel
BuildRequires:  dde-dock-devel
BuildRequires:  dde-qt-dbus-factory-devel
BuildRequires:  dde-session-shell-devel

BuildRequires:  gtest-devel
BuildRequires:  gmock-devel
Obsoletes:      dde-network-utils <= 5.4.15

%description
Deepin desktop-environment - network core files.

%package devel
Summary:        Development package for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}
Obsoletes:      dde-network-utils-devel <= 5.4.15

%description devel
Header files and libraries for %{name}.

%prep
%autosetup

sed -i 's|lib/|%{_lib}/|' dock-network-plugin/CMakeLists.txt

%build
export PATH=%{_qt5_bindir}:$PATH
mkdir build && cd build
%cmake ..
%make_build

%install
cd build
%make_install INSTALL_ROOT=%{buildroot}

%files
%doc README.md
%{_bindir}/dde-network-dialog
%{_prefix}/lib/dde-control-center/
%{_libdir}/dde-dock/
%{_prefix}/lib/dde-session-shell/
%{_libdir}/libdde-network-core.so*
%{_datadir}/dsg/
%{_datadir}/dcc-network-plugin/
%{_datadir}/dde-network-dialog/
%{_datadir}/dock-network-plugin/
%{_datadir}/dss-network-plugin/
/var/lib/polkit-1/localauthority/10-vendor.d/10-network-manager.pkla

%files devel
%{_includedir}/libddenetworkcore/
%{_libdir}/pkgconfig/*.pc

%changelog
* Mon Aug 14 2023 leeffo <liweiganga@uniontech.com> - 1.0.74-1
- init: init package
